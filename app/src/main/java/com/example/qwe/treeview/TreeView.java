package com.example.qwe.treeview;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import java.util.LinkedList;

public class TreeView extends View {
    private Tree tree;

    private RenderedTree treeCache;
    private LinkedList<Shape> shapesCache;
    private Paint paint;
    private Paint strokePaint;

    public TreeView(Context context) {
        this(context, null, 0);
    }

    public TreeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TreeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.tree = null;
        this.treeCache = null;
        this.shapesCache = null;
        this.paint = new Paint();
        this.strokePaint = new Paint();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.shapesCache = null;
        postInvalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.tree == null) {
            return;
        }
        if (this.treeCache == null) {
            this.treeCache = this.tree.render();
        }
        if (this.shapesCache == null) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((Activity) this.getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            float width = displayMetrics.widthPixels;
            float height = displayMetrics.heightPixels;
            float maxRadius = Math.min(width / 20, height / 20);
            float scale = this.treeCache.computeScale(this.getWidth(), this.getHeight(), maxRadius);
            float x = (this.getWidth() - this.treeCache.scaledWidth(scale)) / 2;
            this.shapesCache = this.treeCache.toShapes(new Point(x, 0), scale);
        }

        this.strokePaint.reset();
        this.strokePaint.setColor(Color.BLACK);
        this.strokePaint.setStrokeWidth(5);
        this.strokePaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        for (Shape s : this.shapesCache) {
            if (s.type() == Shape.Type.LINE) {
                s.draw(canvas, this.strokePaint);
            }
        }

        this.paint.reset();
        this.paint.setColor(Color.WHITE);
        this.paint.setStyle(Paint.Style.FILL);
        this.strokePaint.setStyle(Paint.Style.STROKE);
        for (Shape s : this.shapesCache) {
            if (s.type() == Shape.Type.CIRCLE) {
                s.draw(canvas, this.paint);
                s.draw(canvas, this.strokePaint);
            }
        }

        this.paint.reset();
        this.paint.setColor(Color.BLACK);
        for (Shape s : this.shapesCache) {
            if (s.type() == Shape.Type.TEXT) {
                s.draw(canvas, this.paint);
            }
        }
    }

    public void insert(int val) {
        if (this.tree == null) {
            this.tree = new Tree(val);
            this.treeCache = null;
            this.shapesCache = null;
            postInvalidate();
        } else {
            if (this.tree.insert(val)) {
                this.treeCache = null;
                this.shapesCache = null;
                postInvalidate();
            }
        }
    }
}
