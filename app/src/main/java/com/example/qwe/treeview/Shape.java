package com.example.qwe.treeview;

import android.graphics.Canvas;
import android.graphics.Paint;

public interface Shape {
    enum Type {
        LINE, CIRCLE, TEXT,
    }

    void draw(final Canvas canvas, final Paint paint);

    Type type();

    class Line implements Shape {
        private final float x1, y1, x2, y2;

        Line(float _x1, float _y1, float _x2, float _y2) {
            this.x1 = _x1;
            this.x2 = _x2;
            this.y1 = _y1;
            this.y2 = _y2;
        }

        @Override
        public void draw(final Canvas canvas, final Paint paint) {
            canvas.drawLine(this.x1, this.y1, this.x2, this.y2, paint);
        }

        @Override
        public Type type() {
            return Type.LINE;
        }
    }

    class Circle implements Shape {
        private final float left, top, right, bottom;

        Circle(float x, float y, float radius) {
            this.left = x - radius;
            this.top = y - radius;
            this.right = x + radius;
            this.bottom = y + radius;
        }


        @Override
        public void draw(Canvas canvas, Paint paint) {
            canvas.drawArc(this.left, this.top, this.right, this.bottom, 0, 360, false, paint);
        }

        @Override
        public Type type() {
            return Type.CIRCLE;
        }
    }

    class Text implements Shape {
        private final String text;
        private final float x, y;
        private final float fontSize;

        Text(int value, float _x, float _y, float radius) {
            this.x = _x - radius / 2;
            this.y = _y + radius / 4;
            this.text = Integer.toString(value);
            this.fontSize = radius;
        }

        @Override
        public void draw(Canvas canvas, Paint paint) {
            float size = paint.getTextSize();
            paint.setTextSize(this.fontSize);
            canvas.drawText(this.text, this.x, this.y, paint);
            paint.setTextSize(size);
        }

        @Override
        public Type type() {
            return Type.TEXT;
        }
    }
}
