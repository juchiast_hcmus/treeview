package com.example.qwe.treeview;

import java.util.LinkedList;

class Point {
    float x;
    float y;

    Point(float _x, float _y) {
        this.x = _x;
        this.y = _y;
    }

    public Point add(Point p, float scale) {
        return new Point(this.x + p.x * scale, this.y + p.y * scale);
    }
}

class RenderedTree {
    private final float radius = 1;
    private final Point root;
    private final Point left_pos;
    private final RenderedTree left;
    private final Point right_pos;
    private final RenderedTree right;
    private final float width;
    private final float height;
    private final int value;

    RenderedTree(final int val, final RenderedTree l, final RenderedTree r) {
        this.value = val;
        if (l == null && r == null) {
            this.left_pos = null;
            this.left = null;
            this.right_pos = null;
            this.right = null;
            this.root = new Point(this.radius, this.radius);
            this.width = 2 * this.radius;
            this.height = 2 * this.radius;
        } else if (l != null && r == null) {
            this.left = l;
            this.left_pos = new Point(0, 2 * this.radius);
            this.right = null;
            this.right_pos = null;
            this.height = this.left.height + 2 * this.radius;
            this.root = new Point(this.left_pos.x + this.left.width, this.radius);
            this.width = this.root.x + this.radius;
        } else if (l == null && r != null) {
            this.left_pos = null;
            this.left = null;
            this.right = r;
            this.height = this.right.height + 2 * this.radius;
            this.right_pos = new Point(this.radius, 2 * this.radius);
            this.root = new Point(this.radius, this.radius);
            this.width = this.right_pos.x + this.right.width;
        } else {
            this.left = l;
            this.left_pos = new Point(0, 2 * this.radius);
            this.right = r;
            this.right_pos = new Point(this.left_pos.x + this.left.width, 2 * this.radius);
            this.height = Math.max(this.left.height, this.right.height) + 2 * this.radius;
            this.root = new Point(this.right_pos.x, this.radius);
            this.width = this.right_pos.x + this.right.width;
        }
    }

    LinkedList<Shape> toShapes(final Point pos, float scale) {
        final LinkedList<Shape> shapes = new LinkedList<>();
        final Point r = pos.add(this.root, scale);
        shapes.add(new Shape.Circle(r.x, r.y, this.radius * scale));
        shapes.add(new Shape.Text(this.value, r.x, r.y, this.radius * scale));
        if (this.left != null) {
            final Point p = pos.add(this.left_pos, scale).add(this.left.root, scale);
            shapes.add(new Shape.Line(r.x, r.y, p.x, p.y));
            shapes.addAll(this.left.toShapes(pos.add(this.left_pos, scale), scale));
        }
        if (this.right != null) {
            final Point p = pos.add(this.right_pos, scale).add(this.right.root, scale);
            shapes.add(new Shape.Line(r.x, r.y, p.x, p.y));
            shapes.addAll(this.right.toShapes(pos.add(this.right_pos, scale), scale));
        }
        return shapes;
    }

    float computeScale(float width, float height, float maxRadius) {
        final float wScale = width / this.width;
        final float hScale = height / this.height;
        final float rScale = maxRadius / this.radius;
        return Math.min(rScale, Math.min(wScale, hScale));
    }

    float scaledWidth(float scale) {
        return this.width * scale;
    }
}
