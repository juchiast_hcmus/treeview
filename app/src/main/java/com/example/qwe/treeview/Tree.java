package com.example.qwe.treeview;

class Tree {
    private final int value;
    private Tree left;
    private Tree right;

    Tree(final int val) {
        this.value = val;
        this.left = null;
        this.right = null;
    }

    boolean insert(final int val) {
        Tree p = this;
        while (true) {
            if (val == p.value) {
                return false;
            } else if (val < p.value) {
                if (p.left == null) {
                    p.left = new Tree(val);
                    return true;
                } else {
                    p = p.left;
                }
            } else {
                if (p.right == null) {
                    p.right = new Tree(val);
                    return true;
                } else {
                    p = p.right;
                }
            }
        }
    }

    RenderedTree render() {
        RenderedTree rLeft = null;
        if (this.left != null) {
            rLeft = this.left.render();
        }
        RenderedTree rRight = null;
        if (this.right != null) {
            rRight = this.right.render();
        }
        return new RenderedTree(this.value, rLeft, rRight);
    }
}
