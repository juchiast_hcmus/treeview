package com.example.qwe.treeview;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void onInsertButtonClick(View view) {
        EditText t = this.findViewById(R.id.myText);
        TreeView tree = this.findViewById(R.id.myTree);
        tree.insert(Integer.valueOf(t.getText().toString()));
    }
}
